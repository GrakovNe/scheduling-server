package org.grakovne.crypto.chat.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;


import java.io.IOException;

public class GlobalConfig {
    private String passPhrase;

    public GlobalConfig() throws ConfigurationException, IOException {
        Configuration config = new PropertiesConfiguration("system.properties");
        passPhrase = config.getString("passPhrase");
    }

    public String getPassPhrase() {
        return passPhrase;
    }
}
