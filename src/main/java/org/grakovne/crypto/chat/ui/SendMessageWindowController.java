package org.grakovne.crypto.chat.ui;

import org.grakovne.crypto.chat.crypto.MessageCryptoUtils;
import org.grakovne.crypto.chat.network.Server;
import org.grakovne.crypto.chat.network.dto.Client;
import org.grakovne.crypto.chat.network.dto.Message;

public class SendMessageWindowController {
    private Server server;
    private Client client;
    private SendMessageWindowView view;
    private  MessageCryptoUtils cryptoUtils;


    public SendMessageWindowController(Server server, Client client, MessageCryptoUtils cryptoUtils) {
        this.client = client;
        this.server = server;
        this.cryptoUtils = cryptoUtils;

        view = new SendMessageWindowView();
        initViews();
        view.showWindow();
    }

    private void initViews() {
        view.getSendButton().addActionListener(e -> {
            String messageText = view.getTextArea().getText();
            String subjectText = view.getSubjectArea().getText();
            Message message = new Message(cryptoUtils.encrypt(subjectText), cryptoUtils.encrypt(messageText));

            if (null != client) {
                server.sendMessage(client, message);
            } else {
                server.sendMessage(message);
            }

            view.hideWindow();
        });
    }
}
