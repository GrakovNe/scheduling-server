package org.grakovne.crypto.chat.ui;

import org.apache.commons.configuration.ConfigurationException;
import org.grakovne.crypto.chat.config.GlobalConfig;
import org.grakovne.crypto.chat.crypto.MessageCryptoUtils;
import org.grakovne.crypto.chat.network.Server;
import org.grakovne.crypto.chat.network.dto.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MainWindowController {

    private final static Logger logger = LoggerFactory.getLogger(MainWindowController.class);

    private final MainWindowView view;
    private final MessageCryptoUtils cryptoUtils;


    private Server server;
    public MainWindowController() throws IOException, ConfigurationException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        this.view = new MainWindowView();
        cryptoUtils = new MessageCryptoUtils(new GlobalConfig().getPassPhrase());

        initViews();
        view.showWindow();
    }

    private void initViews() {
        view.getStartServerButton().addActionListener(e -> {
                    try {
                        server = new Server(8080);
                        server.startServer();
                    } catch (IOException e1) {
                        logger.error("can't start server");
                    }
                }
        );

        view.getStopServerButton().addActionListener(e -> server.stopServer());

        new Thread(() -> {
            while (true) {
                if (null != server) {
                    DefaultListModel<Client> listModel = new DefaultListModel<>();
                    server.getClients().forEach(listModel::addElement);
                    view.getClients().setModel(listModel);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        view.getSendMessageButton().addActionListener(e -> {
            Client selectedClient = view.getClients().getSelectedValue();
            new SendMessageWindowController(server, selectedClient, cryptoUtils);
        });

    }
}
