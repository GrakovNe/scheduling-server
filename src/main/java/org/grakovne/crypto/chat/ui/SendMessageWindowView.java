package org.grakovne.crypto.chat.ui;

import javax.swing.*;

public class SendMessageWindowView extends JFrame {

    private JButton sendButton;

    private JTextArea subjectArea;
    private JTextArea textArea;

    public SendMessageWindowView() {
        super("Scheduling server");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setBounds(300, 50, 300, 300);
        setLayout(null);

        subjectArea = new JTextArea();
        subjectArea.setBounds(20, 20, 250, 20);
        add(subjectArea);

        textArea = new JTextArea();
        textArea.setBounds(50, 50, 200, 180);
        add(textArea);

        sendButton = new JButton("Send");
        sendButton.setBounds(100, 250, 100, 20);
        add(sendButton);

        setResizable(false);
    }

    public void showWindow() {
        setVisible(true);
    }

    public void hideWindow() {
        setVisible(false);
    }

    public JButton getSendButton() {
        return sendButton;
    }

    public JTextArea getSubjectArea() {
        return subjectArea;
    }

    public JTextArea getTextArea() {
        return textArea;
    }
}
