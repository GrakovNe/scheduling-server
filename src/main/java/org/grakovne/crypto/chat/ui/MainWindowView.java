package org.grakovne.crypto.chat.ui;

import org.grakovne.crypto.chat.network.dto.Client;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public class MainWindowView extends JFrame {

    private JButton startServerButton;
    private JButton stopServerButton;

    private JButton sendMessageButton;

    private JList<Client> clients;

    public JButton getStartServerButton() {
        return startServerButton;
    }

    public JButton getStopServerButton() {
        return stopServerButton;
    }

    public JList<Client> getClients() {
        return clients;
    }

    public JButton getSendMessageButton() {
        return sendMessageButton;
    }

    public MainWindowView() {
        super("Crypto Server");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(300, 50, 605, 620);
        setLayout(null);

        startServerButton = new JButton("Start server");
        startServerButton.setBounds(20, 560, 100, 20);
        add(startServerButton);

        stopServerButton = new JButton("Stop server");
        stopServerButton.setBounds(485, 560, 100, 20);
        add(stopServerButton);

        clients = new JList<>();
        clients.setBounds(20, 40, 565, 500);
        add(clients);

        JLabel clientsLabel = new JLabel("CLIENTS");
        clientsLabel.setBounds(20, 10, 100, 20);
        add(clientsLabel);

        sendMessageButton = new JButton("Send message");
        sendMessageButton.setBounds(252, 560, 100, 20);
        add(sendMessageButton);

        setResizable(false);
    }

    public void showWindow(){
        setVisible(true);
    }
}