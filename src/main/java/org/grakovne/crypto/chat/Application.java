package org.grakovne.crypto.chat;

import org.apache.commons.configuration.ConfigurationException;
import org.grakovne.crypto.chat.config.GlobalConfig;
import org.grakovne.crypto.chat.ui.MainWindowController;

import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Application {
    public static void main(String[] args) throws IOException, ConfigurationException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        new MainWindowController();
    }
}
