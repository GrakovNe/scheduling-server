package org.grakovne.crypto.chat.network.dto;

import java.net.Socket;
import java.time.LocalDateTime;
import java.util.UUID;

public class Client {

    private Socket socket;
    private String login;
    private UUID id;

    private LocalDateTime connectionTimeStamp;
    private LocalDateTime lastActivityTimeStamp;

    public Client(Socket socket) {
        this.socket = socket;
        this.id = UUID.randomUUID();
        this.login = id.toString();

        this.connectionTimeStamp = LocalDateTime.now();
        this.lastActivityTimeStamp = LocalDateTime.now();
    }

    public Socket getSocket() {
        return socket;
    }

    public String getLogin() {
        return login;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getConnectionTimeStamp() {
        return connectionTimeStamp;
    }

    public LocalDateTime getLastActivityTimeStamp() {
        return lastActivityTimeStamp;
    }

    public void setConnectionTimeStamp(LocalDateTime connectionTimeStamp) {
        this.connectionTimeStamp = connectionTimeStamp;
    }

    public void setLastActivityTimeStamp(LocalDateTime lastActivityTimeStamp) {
        this.lastActivityTimeStamp = lastActivityTimeStamp;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return login;
    }
}
