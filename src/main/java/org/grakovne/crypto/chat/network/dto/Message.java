package org.grakovne.crypto.chat.network.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Message implements Serializable {

    private String subject;
    private String message;

    private LocalDateTime timeStamp;

    public Message(String subject, String message) {

        if (null == subject) {
            subject = "";
        } else {
            this.subject = subject;
        }

        if (null == message) {
            message = "";
        } else {
            this.message = message;
        }

        timeStamp = LocalDateTime.now();
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String toString() {
        return "Message{" +
                "subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
