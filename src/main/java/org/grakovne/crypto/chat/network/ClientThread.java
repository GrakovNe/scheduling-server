package org.grakovne.crypto.chat.network;

import com.google.gson.Gson;
import org.apache.commons.configuration.ConfigurationException;
import org.grakovne.crypto.chat.config.GlobalConfig;
import org.grakovne.crypto.chat.crypto.MessageCryptoUtils;
import org.grakovne.crypto.chat.network.dto.Client;
import org.grakovne.crypto.chat.network.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.NoSuchPaddingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class ClientThread extends Thread {

    private final static Logger logger = LoggerFactory.getLogger(ClientThread.class);
    private final static Gson gson = new Gson();

    private final Client client;
    private MessageCryptoUtils cryptoUtils;

    private BufferedReader in;
    private PrintWriter out;

    public ClientThread(Client client) throws IOException, ConfigurationException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        this.client = client;
        cryptoUtils = new MessageCryptoUtils(new GlobalConfig().getPassPhrase());

        in = new BufferedReader(new InputStreamReader(client.getSocket().getInputStream()));
        out = new PrintWriter(client.getSocket().getOutputStream(), true);
    }

    public void sendMessage(Message message) {
        try {
            out.println(new Gson().toJson(message));
            logger.info("message to " + client.getLogin() + " sent successful");
        } catch (Exception e) {
            logger.error("Can't send message to " + client.getLogin());
        }
    }

    public Client getClient() {
        return client;
    }

    public void run() {
        try {
            while (true) {
                String serializedMessage = in.readLine();

                if (null == serializedMessage) {
                    continue;
                }

                Message message = toMessage(serializedMessage);
                receiveMessage(message);

                logger.info(serializedMessage);
            }

        } catch (IOException e) {
            logger.warn("receiving messages from " + client.getLogin() + " is broken");
        }
    }

    private void receiveMessage(Message message) {
        if (message.getSubject().equalsIgnoreCase("login")) {
            this.client.setLogin(message.getMessage());
            logger.info("client with id " + client.getId() + " set login to " + client.getLogin());
            sendMessage(new Message(cryptoUtils.encrypt("hello"), cryptoUtils.encrypt("Hello, " + client.getLogin())));
            return;
        }

        logger.info(message.toString());
    }

    private Message toMessage(String msg) {
        Message message = gson.fromJson(msg, Message.class);

        String decodedSubject = cryptoUtils.decrypt(message.getSubject());
        String decodedText = cryptoUtils.decrypt(message.getMessage());
        Message decodedMessage = new Message(decodedSubject, decodedText);

        logger.info("received message from " + client.getLogin() + " : " + message.toString());
        return decodedMessage;
    }
}
