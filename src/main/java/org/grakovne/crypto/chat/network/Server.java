package org.grakovne.crypto.chat.network;

import org.grakovne.crypto.chat.Application;
import org.grakovne.crypto.chat.exception.NoSuchClientException;
import org.grakovne.crypto.chat.network.dto.Client;
import org.grakovne.crypto.chat.network.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Server {

    private final static Logger logger = LoggerFactory.getLogger(Application.class);

    private final Thread serverThread;
    private final LocalDateTime startedTimeStamp;
    private final Integer serverPort;

    private final ServerSocket socketListener;

    private final Map<Client, ClientThread> clientThreads = new HashMap<>();

    public Server(Integer serverPort) throws IOException {
        this.serverPort = serverPort;
        this.startedTimeStamp = LocalDateTime.now();
        socketListener = new ServerSocket(serverPort);

        serverThread = new Thread(() -> {
            try {
                listenClients();
            } catch (IOException e) {
                logger.info("Cant't process client");
            }
        });

        logger.info("Server started on " + serverPort);
    }

    public void startServer() {
        serverThread.start();
    }

    public void stopServer() {
        serverThread.stop();
        logger.info("Server stopped");
    }

    private void listenClients() throws IOException {

        Socket socket = socketListener.accept();

        try {
            Client client = new Client(socket);
            client.setConnectionTimeStamp(LocalDateTime.now());
            ClientThread clientThread = new ClientThread(client);
            clientThreads.put(client, clientThread);
            clientThread.start();

            logger.info("Client with login " + client.getId() + " connected");
            listenClients();

        } catch (Exception e) {
            logger.info("Cant't read message from client");

        }
    }

    public LocalDateTime getStartedTimeStamp() {
        return startedTimeStamp;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void sendMessage(List<Client> clients, Message message) {
        checkIfNoClients();
        clients.forEach(client -> sendMessage(client, message));
    }

    public void sendMessage(Message message) {
        checkIfNoClients();
        clientThreads.forEach((key, value) -> sendMessage(key, message));
    }

    public List<Client> getClients() {
        return clientThreads
                .entrySet()
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public void sendMessage(Client client, Message message) {
        ClientThread thread = clientThreads.get(client);
        checkIfNoClient(thread, client);
        thread.sendMessage(message);
    }

    private void checkIfNoClients() {
        if (clientThreads.isEmpty()) {
            logger.warn("no clients connected. Sending messages is useless.");
        }
    }

    private void checkIfNoClient(ClientThread thread, Client client) {
        if (null == thread) {
            throw new NoSuchClientException(client);
        }
    }
}
