package org.grakovne.crypto.chat.exception;

import org.grakovne.crypto.chat.network.dto.Client;

public class NoSuchClientException extends RuntimeException {
    public NoSuchClientException(Client client) {
        super("no such client with login " + client.getLogin());
    }
}
